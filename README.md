## Implementation notes

## CrazyAirResponse.java and ToughJetResponse.java

	These 2 classes were changed to implement the interface SupplierResponse in order to implement the visitor pattern.
	I also changed the date fields to LocalDate to work better with the dates.

## CrazyAirController.java and ToughJetController.java
	
	I created these 2 mock controllers to test the suppliers APIs.

## Tests:
	
	I didn't have enough time to implement unit tests.

## Notes:

	To make these application production ready I would implement a security layer to the API. Like an OAuth 2 protocol.