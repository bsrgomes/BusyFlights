package com.travix.medusa.busyflights.mapper;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

public interface FlightSupplierResponseVisitor<T> {

  T visit(CrazyAirResponse in);

  T visit(ToughJetResponse in);

}
