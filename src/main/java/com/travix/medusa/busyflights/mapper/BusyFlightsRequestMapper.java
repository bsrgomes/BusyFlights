package com.travix.medusa.busyflights.mapper;

import org.springframework.stereotype.Component;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;

@Component
public class BusyFlightsRequestMapper {

  public CrazyAirRequest toCrazyAirRequest(BusyFlightsRequest in) {
    return new CrazyAirRequest(in.getOrigin(), in.getDestination(), in.getDepartureDate(),
        in.getReturnDate(), in.getNumberOfPassengers());
  }

  public ToughJetRequest toToughJetRequest(BusyFlightsRequest in) {
    return new ToughJetRequest(in.getOrigin(), in.getDestination(), in.getDepartureDate(),
        in.getReturnDate(), in.getNumberOfPassengers());
  }
}
