package com.travix.medusa.busyflights.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Component;

import com.travix.medusa.busyflights.domain.FlightSupplier;
import com.travix.medusa.busyflights.domain.SupplierResponse;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.util.DateUtils;

@Component
public class FlightSupplierResponseMapper implements
    FlightSupplierResponseVisitor<BusyFlightsResponse> {

  private static final int DECIMALS_COUNT = 2;

  public BusyFlightsResponse map(SupplierResponse in) {
    return in.accept(this);
  }

  @Override
  public BusyFlightsResponse visit(CrazyAirResponse in) {
    BigDecimal fare =
        BigDecimal.valueOf(in.getPrice()).setScale(DECIMALS_COUNT, RoundingMode.HALF_EVEN);
    String departureDate = DateUtils.fromLocalDateTimeToDateTime(in.getDepartureDate());
    String arrivalDate = DateUtils.fromLocalDateTimeToDateTime(in.getArrivalDate());

    return new BusyFlightsResponse(in.getAirline(), FlightSupplier.CrazyAir.toString(), fare,
        in.getDepartureAirportCode(), in.getDestinationAirportCode(), departureDate, arrivalDate);
  }

  @Override
  public BusyFlightsResponse visit(ToughJetResponse in) {
    double finalPrice =
        in.getBasePrice() - (in.getBasePrice() * in.getDiscount() / 100) + in.getTax();
    BigDecimal fare =
        BigDecimal.valueOf(finalPrice).setScale(DECIMALS_COUNT, RoundingMode.HALF_EVEN);

    String departureDate = DateUtils.fromInstantToDateTime(in.getOutboundDateTime());
    String arrivalDate = DateUtils.fromInstantToDateTime(in.getInboundDateTime());

    return new BusyFlightsResponse(in.getCarrier(), FlightSupplier.ToughJet.toString(), fare,
        in.getDepartureAirportName(), in.getArrivalAirportName(), departureDate, arrivalDate);
  }

}
