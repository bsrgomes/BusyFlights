package com.travix.medusa.busyflights.domain;

import com.travix.medusa.busyflights.mapper.FlightSupplierResponseVisitor;

public interface SupplierResponse {
  <T> T accept(FlightSupplierResponseVisitor<T> visitor);
}
