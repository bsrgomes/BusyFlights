package com.travix.medusa.busyflights.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.travix.medusa.busyflights.domain.SupplierResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

@Component
public class FlightConnector extends RestTemplate {

  @Value("${search.url.crazy-air}")
  private String crazyAirUrl;

  @Value("${search.url.tough-jet}")
  private String toughJetUrl;

  public SupplierResponse[] crazyAirFlightsSearch(@Valid CrazyAirRequest request) {
    return this.getForObject(crazyAirUrl, CrazyAirResponse[].class,
        new Object[] {request.getOrigin(), request.getDestination(), request.getDepartureDate(),
            request.getReturnDate(), request.getPassengerCount()});
  }

  public SupplierResponse[] toughJetFlightsSearch(@Valid ToughJetRequest request) {
    return this.getForObject(toughJetUrl, ToughJetResponse[].class, new Object[] {
        request.getFrom(), request.getTo(), request.getInboundDate(), request.getOutboundDate(),
        request.getNumberOfAdults()});
  }

}
