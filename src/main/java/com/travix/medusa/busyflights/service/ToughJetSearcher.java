package com.travix.medusa.busyflights.service;

import org.springframework.stereotype.Component;

import com.travix.medusa.busyflights.domain.SupplierResponse;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;

@Component
public class ToughJetSearcher extends SupplierSearcher {

  @Override
  protected SupplierResponse[] search(BusyFlightsRequest in) {
    return this.flightConnector.toughJetFlightsSearch(this.busyFlightsRequestTransformer
        .toToughJetRequest(in));
  }

}
