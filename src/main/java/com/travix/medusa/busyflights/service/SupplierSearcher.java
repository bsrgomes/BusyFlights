package com.travix.medusa.busyflights.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.travix.medusa.busyflights.domain.SupplierResponse;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.mapper.BusyFlightsRequestMapper;
import com.travix.medusa.busyflights.rest.FlightConnector;

@Component
public abstract class SupplierSearcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(BusyFlightsRequestMapper.class);

  @Autowired
  protected FlightConnector flightConnector;

  @Autowired
  protected BusyFlightsRequestMapper busyFlightsRequestTransformer;

  public SupplierResponse[] searchOnSupplier(BusyFlightsRequest request) {
    try {
      return this.search(request);
    } catch (Exception e) {
      LOGGER.error("Flight search error: " + e);
    }
    return new SupplierResponse[0];
  }

  protected abstract SupplierResponse[] search(BusyFlightsRequest request);

}
