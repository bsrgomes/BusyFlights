package com.travix.medusa.busyflights.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.travix.medusa.busyflights.domain.SupplierResponse;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.mapper.FlightSupplierResponseMapper;
import com.travix.medusa.busyflights.sort.BusyFlightsComparator;

@Service
public class BusyFlightsServiceImpl implements BusyFlightsService {

  @Autowired
  private List<SupplierSearcher> supplierSearchers;

  @Autowired
  private FlightSupplierResponseMapper flightSupplierResponseMapper;

  @Autowired
  private BusyFlightsComparator busyFlightsComparator;


  @Override
  public List<BusyFlightsResponse> searchFlights(BusyFlightsRequest request) {
    List<CompletableFuture<SupplierResponse[]>> futures =
        this.supplierSearchers.stream()
            .map(searcher -> this.createCompletableFuture(searcher, request))
            .collect(Collectors.toList());

    return futures.stream().map(CompletableFuture::join).flatMap(Arrays::stream)
        .map(response -> this.flightSupplierResponseMapper.map(response))
        .sorted(busyFlightsComparator).collect(Collectors.toList());
  }

  private CompletableFuture<SupplierResponse[]> createCompletableFuture(SupplierSearcher searcher,
      BusyFlightsRequest request) {
    Supplier<SupplierResponse[]> supplier = () -> {
      return searcher.search(request);
    };
    return CompletableFuture.supplyAsync(supplier);
  }

}
