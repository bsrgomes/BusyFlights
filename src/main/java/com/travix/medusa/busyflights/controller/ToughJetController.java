package com.travix.medusa.busyflights.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;


@RestController
public class ToughJetController {

  @RequestMapping(value = "/toughjet/flights", method = RequestMethod.GET)
  public List<ToughJetResponse> toughJetFlights(BusyFlightsRequest request) {
    List<ToughJetResponse> out = new ArrayList<ToughJetResponse>();

    out.add(new ToughJetResponse("Copa", 200.21, 100.32, 20, "JFK", "LAX",
        "2017-07-08T09:28:27.141Z", "2017-07-09T09:28:27.141Z"));
    out.add(new ToughJetResponse("Copa", 250.21, 110.32, 15, "JFK", "LAX",
        "2017-07-08T09:28:27.141Z", "2017-07-09T09:28:27.141Z"));
    out.add(new ToughJetResponse("UnitedAirlines", 99.21, 40.32, 5, "JFK", "LAX", "2017-07-08T09:28:27.141Z",
        "2017-07-09T09:28:27.141Z"));
    out.add(new ToughJetResponse("UnitedAirlines", 120.21, 45.32, 7, "JFK", "LAX", "2017-07-08T09:28:27.141Z",
        "2017-07-09T09:28:27.141Z"));
    out.add(new ToughJetResponse("Delta", 77.21, 33.32, 15, "JFK", "LAX",
        "2017-07-08T09:28:27.141Z", "2017-07-09T09:28:27.141Z"));
    out.add(new ToughJetResponse("Delta", 125.21, 70.32, 35, "JFK", "LAX",
        "2017-07-08T09:28:27.141Z", "2017-07-09T09:28:27.141Z"));

    return out;
  }

}
